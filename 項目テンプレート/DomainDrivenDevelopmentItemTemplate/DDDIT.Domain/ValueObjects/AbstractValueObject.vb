﻿Namespace ValueObjects

    Public MustInherit Class AbstractValueObject(Of T As AbstractValueObject(Of T))
        Public Overrides Function Equals(ByVal obj As Object) As Boolean
            Dim vo = TryCast(obj, T)

            If vo Is Nothing Then
                Return False
            End If

            Return EqualsCore(vo)
        End Function

        Public Shared Operator =(ByVal vo1 As AbstractValueObject(Of T), ByVal vo2 As AbstractValueObject(Of T)) As Boolean
            Return Equals(vo1, vo2)
        End Operator

        Public Shared Operator <>(ByVal vo1 As AbstractValueObject(Of T), ByVal vo2 As AbstractValueObject(Of T)) As Boolean
            Return Not Equals(vo1, vo2)
        End Operator

        Protected MustOverride Function EqualsCore(ByVal other As T) As Boolean

        Public Overrides Function ToString() As String
            Return MyBase.ToString()
        End Function

        Public Overrides Function GetHashCode() As Integer
            Return MyBase.GetHashCode()
        End Function

    End Class

End Namespace