﻿Namespace ValueObjects

    ''' <summary>
    ''' $safeitemname$クラス
    ''' </summary>
    Public Class ValueObject
        Inherits AbstractValueObject(Of ValueObject)

        ''' <summary>値</summary>
        Public ReadOnly Property Value As DBに合わせた型を入力してください

        ''' <summary>
        ''' コンストラクタ
        ''' </summary>
        ''' <param name="value">値</param>
        Public Sub New(value As DBに合わせた型を入力してください)
            Me.Value = value
        End Sub

        ''' <summary>
        ''' EqualsCore
        ''' </summary>
        ''' <param name="other"></param>
        ''' <returns>等しい場合True</returns>
        Protected Overrides Function EqualsCore(other As ValueObject) As Boolean
            Return Value = other.Value
        End Function
    End Class

End Namespace