﻿Imports System.Configuration

''' <summary>
''' 共有クラス。
''' アプリ全体で共有したい定数・変数を実装してください。
''' </summary>
''' <example>
''' Public Shared Property LoginId As String = String.Empty
''' Public Shared ReadOnly Property ExportPath As String = ConfigurationManager.AppSettings("ExportPath")
''' </example>
Public Class [Shared]

End Class
